package model;

import java.util.ArrayList;

public class Time {
	private String pais;
	private	ArrayList <Vitorias> vitoriasTime;
	private	ArrayList <Jogador> jogadoresTime;
	
	
	public String getPais() {
		return pais;
		
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public ArrayList<Vitorias> getvitoriasTime() {
		return vitoriasTime;
	}
	public void setvitoriasTime(ArrayList<Vitorias> vitoriasTime) {
		this.vitoriasTime = vitoriasTime;
	}
	
	public ArrayList<Jogador> getJogadoresTime() {
		return jogadoresTime;
	}
	public void setJogadoresTime(ArrayList<Jogador> jogadoresTime) {
		this.jogadoresTime = jogadoresTime;
	}
	
	
		
}
