package model;

import java.util.ArrayList;

public class Jogador extends Atleta {
	
	Time time;
	double numGols;
	ArrayList<Vitorias> listaVitorias;
	char peBom; // D = destro, E = canhoto, A= ambidestro
	String posicao;
	
	public Jogador(String nome){
		super(nome);
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public double getNumGols() {
		return numGols;
	}

	public void setNumGols(double numGols) {
		this.numGols = numGols;
	}

	public ArrayList<Vitorias> getVitorias() {
		return listaVitorias;
	}

	public void setVitorias(ArrayList<Vitorias> listaVitorias) {
		this.listaVitorias = listaVitorias;
	}

	public char getPeBom() {
		return peBom;
	}

	public void setPeBom(char peBom) {
		this.peBom = peBom;
	}

	public String getPosicao() {
		return posicao;
	}

	public void setPosicao(String posicao) {
		this.posicao = posicao;
	}
	
	public String getPePreferencia( char peBom ){
		if(peBom == 'D'){
			return "destro";
		}
		if(peBom == 'E'){
			return "canhoto";
		}
		if(peBom == 'A'){
			return "ambidestro";
		}
		else
			return "";
	}
	

}
