package model;


public class Atleta {
	
	private String nome;
	private int idade;
	public Endereco endereco;
	private Double altura;
	private Double peso;
	
	
	public Atleta(String nome) {
	this.nome = nome;
	this.endereco = new Endereco();
	}
	public Double getAltura() {
		return altura;
		}
	public void setAltura(Double altura) {
	this.altura = altura;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}		
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public int getIdade() {
			return idade;
	}
	public void setIdade(int idade) {
			this.idade = idade;
	}
	
	public void mostraAtleta(){
		System.out.println("nome :" + this.nome);
		System.out.println("idade :" + this.idade);
		this.endereco.mostraEndereco();
		System.out.println("altura :" + this.altura);
		System.out.println("peso :" + this.peso);
		
	}
	
}
