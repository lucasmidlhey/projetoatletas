package model;

public class Endereco {
	
	String cep;
	String pais;
	String cidade;
	
	
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public void mostraEndereco(){
		
		System.out.println("cidade :" + this.cidade);
		System.out.println("pais :" + this.pais);
		System.out.println("cep :" + this.cep);
	}
}


