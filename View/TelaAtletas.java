package view;

import java.util.Scanner;
import model.Atleta;
import model.Time;
import model.Endereco;
import model.Jogador;
import controler.ControladorAtleta;
import controler.ControladorVitorias;

public class TelaAtletas {

	public static void main(String[] args) {
		
		int escolha = -1;
		Scanner leitor = new Scanner(System.in);
		
		while(escolha != 0){
			
			System.out.println("\t Cadastro De Atletas:\n\n\n\t 1 - Adicionar Atleta \n 2- Remover Atleta\n 3- Buscar Atleta");
			escolha = leitor.nextInt();
			Time umEndereco;
			String nomeAtleta;
			switch(escolha){
			
			case 1:
				System.out.println("Digite o nome do Atleta :");
				Atleta umAtleta = new Atleta(leitor.next());
				System.out.println("Digite a altura do Atleta :");
				umAtleta.setAltura(leitor.nextDouble());
				System.out.println("Digite o peso do Atleta :");
				umAtleta.setPeso(leitor.nextDouble());
				
				System.out.println("Digite a idade do Atleta :");
				umAtleta.setIdade(leitor.nextByte());
				break;
			
			case 2:
				umControlador.exibirAtletas();
				
				System.out.println("\nDigite o Nome do Atleta: ");
				nomeAtleta = leitor.next();
				umControlador.pesquisarNome(nomeAtleta);
				umControlador.remover(umAtleta);
				break;
			
			case 3:
				umControlador.exibirAtletas();
				System.out.println("Digite o nome do Atleta: ");
				nomeAtleta = leitor.next();
				
				umAtleta = umControlador.pesquisarNome(nomeAtleta);
				System.out.println("Nome: " + umAtleta.getNome());
				System.out.println("Idade: " + umAtleta.getIdade());
				System.out.println("Peso: " + umAtleta.getPeso());
				
				
				System.out.println("Cidade: "+ umEndereco.getCidade());
			
				System.out.println("Pais: "+ umEndereco.getPais());
				
				break;
			
			case -1:
			escolha = -1;
			break;
			default :
				System.out.println("Opcao Incorreta ! Tente Novamente.");

			
			}
			
			
			
		}
	}
	
	
	
}
